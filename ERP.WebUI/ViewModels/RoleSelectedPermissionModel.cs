﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ERP.WebUI
{
    public class RoleSelectedPermissionModel
    {
        public RoleSelectedPermissionModel()
        {
            this.HeaderPermissionList = new List<PermissionSelectedModel>();
            this.ModulePermissionDataList = new List<ModulePermissionModel>();
        }

        public int RoleId { get; set; }
        public string OldModulePermission { get; set; }
        public string NewModulePermission { get; set; }

        public List<PermissionSelectedModel> HeaderPermissionList { get; set; }
        public List<ModulePermissionModel> ModulePermissionDataList { get; set; }
    }

    public class ModulePermissionModel
    {
        public ModulePermissionModel()
        {
            this.PermissionDataList = new List<PermissionSelectedModel>();
        }

        public int ModuleId { get; set; }

        public int? ParentId { get; set; }

        public string ModuleName { get; set; }

        public string Code { get; set; }

        public bool Selected { get; set; }

        public List<PermissionSelectedModel> PermissionDataList { get; set; }
    }

    public class PermissionSelectedModel
    {
        public int PermissionId { get; set; }

        public string PermissionName { get; set; }

        public int OrderSort { get; set; }

        public bool Selected { get; set; }

        public bool Enabled { get; set; }

    }
}