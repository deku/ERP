﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ERP.WebUI
{
    /// <summary>
    /// 选中菜单
    /// </summary>
    public class RoleSelectedModuleModel
    {
        public RoleSelectedModuleModel()
        {
            this.ModuleDataList = new List<ModuleDataModel>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string oldModulePermission { get; set; }

        public List<ModuleDataModel> ModuleDataList { get; set; }
    }


    public class ModuleDataModel
    {
        public int ModuleId { get; set; }
        public int? ParentId { get; set; }
        public string ModuleName { get; set; }
        public string Code { get; set; }
        public bool Selected { get; set; }
    }
}