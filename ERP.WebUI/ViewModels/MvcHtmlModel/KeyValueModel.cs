﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ERP.WebUI
{
    public class KeyValueModel
    {
        public string Text { get; set; }
        public string Disable { get; set; }
        public string Value { get; set; }
    }
}