﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ERP.WebUI
{
    public enum DisplayType
    {
        RadioList,
        CheckList,
        DropdownList,
        Other
    }
}