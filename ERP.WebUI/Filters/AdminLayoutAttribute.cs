﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ERP.WebUI
{
    /// <summary>
    /// 页面布局
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class AdminLayoutAttribute : ActionFilterAttribute
    {
        public UserRepository userRepository = new UserRepository();
        public RoleRepository roleRepository = new RoleRepository();
        public ModuleRepository moduleRepository = new ModuleRepository();
        public PermissionRepository permissionRepository = new PermissionRepository();
        public ModulePermissionRepository modulePermissionRepository = new ModulePermissionRepository();
        public RoleModulePermissionRepository roleModulePermissionRepository = new RoleModulePermissionRepository();

        public AdminLayoutAttribute()
        {
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = filterContext.HttpContext.Session["CurrentUser"] as User;
            if (user == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { Controller = "Login", Action = "Index" }));
            }
            ////if (filterContext.HttpContext.Session != null)
            ////{
            ////    if (filterContext.HttpContext.Session.IsNewSession)
            ////    {
            ////        var sessionCookie = filterContext.HttpContext.Request.Headers["Cookie"];
            ////        if ((sessionCookie != null) && (sessionCookie.IndexOf("ASP.NET_SessionId", StringComparison.OrdinalIgnoreCase) >= 0))
            ////        {
            ////        }
            ////    }
            ////}
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var user = filterContext.HttpContext.Session["CurrentUser"] as User;

            if (user != null)
            {
                //顶部菜单
                ((ViewResult)filterContext.Result).ViewBag.LoginName = user.LoginName;

                //左侧菜单
                ((ViewResult)filterContext.Result).ViewBag.SidebarMenuModel = InitSidebarMenu(user);

                //面包屑
                ((ViewResult)filterContext.Result).ViewBag.BreadCrumbModel = InitBreadCrumb(filterContext);

                //按钮
                InitButton(user, filterContext);
            }
        }

        private List<SidebarMenuModel> InitSidebarMenu(User user)
        {
            UserRoleRepository userRoleRepository=new UserRoleRepository();
            var entity = userRoleRepository.UserRoles.Where(t => t.UserId == user.Id&&t.IsDeleted==false).Select(t => t.RoleId);

            List<int> RoleIds = entity.ToList();

            var model = new List<SidebarMenuModel>();

            //取出所有选中的节点
            var parentModuleIdList = roleModulePermissionRepository.RoleModulePermissions.Where(t => RoleIds.Contains(t.RoleId) && t.PermissionId == null && t.IsDeleted == false).Select(t => t.ModuleId).Distinct();
            roleModulePermissionRepository = new RoleModulePermissionRepository();
            var childModuleIdList = roleModulePermissionRepository.RoleModulePermissions.Where(t => RoleIds.Contains(t.RoleId) && t.PermissionId != null && t.IsDeleted == false).Select(t => t.ModuleId).Distinct();

            foreach (var pmId in parentModuleIdList)
            {
                //取出父菜单
                var parentModule = moduleRepository.Modules.Where(t => t.Id == pmId).FirstOrDefault();
                if (parentModule != null)
                {
                    var sideBarMenu = new SidebarMenuModel
                    {
                        Id = parentModule.Id,
                        ParentId = parentModule.ParentId,
                        Name = parentModule.Name,
                        Code = parentModule.Code,
                        Icon = parentModule.Icon,
                        LinkUrl = parentModule.LinkUrl,
                    };

                    //取出子菜单
                    foreach (var cmId in childModuleIdList)
                    {
                        var childModule = moduleRepository.Modules.Where(t => t.Id == cmId).FirstOrDefault();
                        if (childModule != null && childModule.ParentId == sideBarMenu.Id)
                        {
                            var childSideBarMenu = new SidebarMenuModel
                            {
                                Id = childModule.Id,
                                ParentId = childModule.ParentId,
                                Name = childModule.Name,
                                Code = childModule.Code,
                                Icon = childModule.Icon,
                                Area = childModule.Area,
                                Controller = childModule.Controller,
                                Action = childModule.Action
                            };
                            sideBarMenu.ChildMenuList.Add(childSideBarMenu);
                        }
                    }

                    //子菜单排序
                    sideBarMenu.ChildMenuList = sideBarMenu.ChildMenuList.OrderBy(t => t.Code).ToList();
                    model.Add(sideBarMenu);
                }
                //父菜单排序
                model = model.OrderBy(t => t.Code).ToList();
            }

            return model;
        }

        private BreadCrumbNavModel InitBreadCrumb(ResultExecutingContext filterContext)
        {
            var area = filterContext.RouteData.DataTokens.ContainsKey("area") ? filterContext.RouteData.DataTokens["area"].ToString().ToLower() : string.Empty;
            var controller = filterContext.RouteData.Values["controller"].ToString().ToLower();
            var action = filterContext.RouteData.Values["action"].ToString().ToLower();

            string linkUrl = string.Format("{0}/{1}", controller, action);

            var model = new BreadCrumbNavModel();

            var indexModel = new BreadCrumbModel
            {
                Name = "首页",
                Icon = "icon-home",
                IsParent = false,
                IsIndex = true
            };

            if (controller == "home" && action == "index")
            {
                model.CurrentName = "首页";
            }

            model.BreadCrumbList.Add(indexModel);

            var module = moduleRepository.Modules.Where(t => t.LinkUrl.ToLower().Contains(linkUrl) && t.IsDeleted == false && t.Enabled == true).FirstOrDefault();

            if (module != null)
            {
                //有父菜单
                if (module.ParentModule != null)
                {
                    var parentModel = new BreadCrumbModel
                    {
                        IsParent = true,
                        Name = module.ParentModule.Name,
                        Icon = module.ParentModule.Icon
                    };
                    model.BreadCrumbList.Add(parentModel);
                }

                var currentModel = new BreadCrumbModel
                {
                    IsParent = false,
                    Name = module.Name,
                    Icon = ""
                };

                model.CurrentName = currentModel.Name;
                model.BreadCrumbList.Add(currentModel);

                ((ViewResult)filterContext.Result).ViewBag.CurrentTitle = module.Name;
            }
            return model;
        }

        private void InitButton(User user, ResultExecutingContext filterContext)
        {
            var roleIds = user.UserRole.Select(t => t.RoleId);
            var controller = filterContext.RouteData.Values["controller"].ToString().ToLower();
            var action = filterContext.RouteData.Values["action"].ToString().ToLower();
            var module = moduleRepository.Modules.Where(t => t.Controller.ToLower() == controller).FirstOrDefault();
            if (module != null)
            {
                var permissionIds = roleModulePermissionRepository.RoleModulePermissions.Where(t => roleIds.Contains(t.RoleId) && t.ModuleId == module.Id).Select(t => t.PermissionId).Distinct();
                foreach (var permissionId in permissionIds)
                {
                    var entity = permissionRepository.Permissions.Where(t => t.Id == permissionId && t.Enabled == true && t.IsDeleted == false).FirstOrDefault();
                    if (entity != null)
                    {
                        var btnButton = new ButtonModel
                        {
                            Icon = entity.Icon,
                            Text = entity.Name
                        };
                        if (entity.Code.ToLower() == "create")
                        {
                            ((ViewResult)filterContext.Result).ViewBag.Create = btnButton;
                        }
                        else if (entity.Code.ToLower() == "edit")
                        {
                            ((ViewResult)filterContext.Result).ViewBag.Edit = btnButton;
                        }
                        else if (entity.Code.ToLower() == "delete")
                        {
                            ((ViewResult)filterContext.Result).ViewBag.Delete = btnButton;
                        }
                        else if (entity.Code.ToLower() == "setbutton")
                        {
                            ((ViewResult)filterContext.Result).ViewBag.SetButton = btnButton;
                        }
                        else if (entity.Code.ToLower() == "setpermission")
                        {
                            ((ViewResult)filterContext.Result).ViewBag.SetPermission = btnButton;
                        }
                        else if (entity.Code.ToLower() == "changepwd")
                        {
                            ((ViewResult)filterContext.Result).ViewBag.ChangePwd = btnButton;
                        }
                        else if (entity.Code.ToLower() == "deleteall")
                        {
                            ((ViewResult)filterContext.Result).ViewBag.DeleteAll = btnButton;
                        }
                    }
                }
            }
        }
    }
}