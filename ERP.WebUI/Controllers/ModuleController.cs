﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace ERP.WebUI.Controllers
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ModuleController : BaseController
    {
        protected ModuleRepository moduleRepository = new ModuleRepository();
        [AdminLayout]
        public ActionResult Index()
        {
            var model = new ModuleModel();

            #region 父菜单列表
            var parentModuleData = moduleRepository.Modules.Where(t => t.ParentId == null && t.IsMenu == true && t.Enabled == true && t.IsDeleted == false)
            .Select(t => new ModuleModel
            {
                Id = t.Id,
                Name = t.Name
            });
            foreach (var item in parentModuleData)
            {
                model.Search.ParentModuleItems.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
            }
            #endregion

            return View(model.Search);
        }

        [AdminPermission(PermissionCustomMode.Ignore)]
        public ActionResult List(DataTableParameter param)
        {
            int total = moduleRepository.Modules.Count(t => t.IsDeleted == false);

            //构建查询表达式
            var expr = BuildSearchCriteria();
            //父菜单
            var parentModuleData = moduleRepository.Modules.Where(t => t.ParentId == null && t.IsMenu == true && t.Enabled == true && t.IsDeleted == false);

            var filterResult = moduleRepository.Modules.Where(expr).Select(t => new ModuleModel
            {
                Id = t.Id,
                Name = "<i class='" + t.Icon + "'></i> " + t.Name,
                Code = t.Code,
                ParentName = parentModuleData.Where(a => a.Id == t.ParentId).Select(a => a.Name).FirstOrDefault(),
                LinkUrl = t.LinkUrl,
                OrderSort = t.OrderSort,
                IsMenu = t.IsMenu,
                Enabled = t.Enabled
            }).OrderBy(t => t.Code).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();

            int sortId = param.iDisplayStart + 1;

            var result = from c in filterResult
                         select new[]
                             {
                                 sortId++.ToString(), 
                                 c.Name,
                                 c.Code,
                                 c.ParentName,                            
                                 c.LinkUrl, 
                                 c.OrderSort.ToString(),
								 c.MenuText,
                                 c.EnabledText, 
                                 c.Id.ToString()
                             };

            return Json(new
            {
                sEcho = param.sEcho,
                iDisplayStart = param.iDisplayStart,
                iTotalRecords = total,
                iTotalDisplayRecords = total,
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }

        #region 构建查询表达式
        /// <summary>
        /// 构建查询表达式
        /// </summary>
        /// <returns></returns>
        private Expression<Func<Module, Boolean>> BuildSearchCriteria()
        {
            DynamicLambda<Module> bulider = new DynamicLambda<Module>();
            Expression<Func<Module, Boolean>> expr = null;
            if (!string.IsNullOrEmpty(Request["Name"]))
            {
                var data = Request["Name"].Trim();
                Expression<Func<Module, Boolean>> tmp = t => t.Name.Contains(data);
                expr = bulider.BuildQueryAnd(expr, tmp);
            }
            if (!string.IsNullOrEmpty(Request["LinkUrl"]))
            {
                var data = Request["LinkUrl"].Trim();
                Expression<Func<Module, Boolean>> tmp = t => t.LinkUrl.Contains(data);
                expr = bulider.BuildQueryAnd(expr, tmp);
            }
            if (!string.IsNullOrEmpty(Request["ParentId"]) && Request["ParentId"] != "0")
            {
                var data = Convert.ToInt32(Request["ParentId"]);
                Expression<Func<Module, Boolean>> tmp = t => (t.Id == data) || (t.ParentId == data);
                expr = bulider.BuildQueryAnd(expr, tmp);
            }
            if (Request["IsMenu"] == "0" || Request["IsMenu"] == "1")
            {
                var data = Request["IsMenu"] == "1" ? true : false;
                Expression<Func<Module, Boolean>> tmp = t => t.IsMenu == data;
                expr = bulider.BuildQueryAnd(expr, tmp);
            }
            if (Request["Enabled"] == "0" || Request["Enabled"] == "1")
            {
                var data = Request["Enabled"] == "1" ? true : false;
                Expression<Func<Module, Boolean>> tmp = t => t.Enabled == data;
                expr = bulider.BuildQueryAnd(expr, tmp);
            }

            Expression<Func<Module, Boolean>> tmpSolid = t => t.IsDeleted == false;
            expr = bulider.BuildQueryAnd(expr, tmpSolid);

            return expr;
        }

        #endregion

        [AdminLayout]
        public ActionResult Create()
        {
            var model = new ModuleModel();
            InitParentModule(model);

            return View(model);
        }

        [HttpPost]
        [AdminOperateLog]
        public ActionResult Create(ModuleModel model)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(model.LinkUrl) && model.LinkUrl.Split('/').Length == 2)
                {
                    string[] link = model.LinkUrl.Split('/');
                    model.Controller = link[0];
                    model.Action = link[1];
                }
                this.CreateBaseData<ModuleModel>(model);
                OperationResult result = moduleRepository.Insert(model);
                if (result.ResultType == OperationResultType.Success)
                {
                    return Json(result);
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        [AdminLayout]
        public ActionResult Edit(int Id)
        {
            var model = new ModuleModel();
            var entity = moduleRepository.Modules.FirstOrDefault(t => t.Id == Id);
            if (null != entity)
            {
                model = new ModuleModel
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Code = entity.Code,
                    Icon = entity.Icon,
                    ParentId = entity.ParentId,
                    LinkUrl = entity.LinkUrl,
                    OrderSort = entity.OrderSort,
                    IsMenu = entity.IsMenu,
                    Enabled = entity.Enabled
                };
                InitParentModule(model);

            }
            return View(model);
        }

        [HttpPost]
        [AdminOperateLog]
        public ActionResult Edit(ModuleModel model)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(model.LinkUrl) && model.LinkUrl.Split('/').Length == 2)
                {
                    string[] link = model.LinkUrl.Split('/');
                    model.Controller = link[0];
                    model.Action = link[1];
                }
                this.UpdateBaseData<ModuleModel>(model);
                OperationResult result = moduleRepository.Update(model);
                if (result.ResultType == OperationResultType.Success)
                {
                    return Json(result);
                }
                else
                {
                    InitParentModule(model);
                    return View(model);
                }
            }
            else
            {
                InitParentModule(model);
                return View(model);
            }
        }

        [AdminLayout]
        public ActionResult SetButton(int Id)
        {
            var module = moduleRepository.Modules.FirstOrDefault(t => t.Id == Id);
            var model = new PermissionButtonModel
            {
                ModuleId = module.Id,
                ModuleName = module.Name
            };
            PermissionRepository permissionRepository = new PermissionRepository();
            model.ButtonList = permissionRepository.GetKeyValueList();
            //Selected Button 
            ModulePermissionRepository modulePermissionRepository = new ModulePermissionRepository();
            var modulePermission = modulePermissionRepository.ModulePermissions.Where(t => t.ModuleId == module.Id && t.IsDeleted == false);
            foreach (var item in modulePermission)
            {
                model.SelectedButtonList.Add(item.PermissionId);
            }
            return View(model);
        }

        [HttpPost]
        [AdminOperateLog]
        public ActionResult SetButton(PermissionButtonModel model)
        {
            if (ModelState.IsValid)
            {
                ModulePermissionRepository modulePermissionRepository = new ModulePermissionRepository();
                OperationResult result = modulePermissionRepository.SetButton(model);
                if (result.ResultType == OperationResultType.Success)
                {
                    return Json(result);
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        [AdminOperateLog]
        public ActionResult Delete(int Id)
        {
            OperationResult result = moduleRepository.Delete(Id);
            return Json(result);
        }


        /// <summary>
        /// 父菜单列表
        /// </summary>
        /// <param name="model"></param>
        private void InitParentModule(ModuleModel model)
        {
            var parentModuleData = moduleRepository.Modules.Where(t => t.ParentId == null && t.IsMenu == true && t.Enabled == true && t.IsDeleted == false)
                .Select(t => new ModuleModel
                {
                    Id = t.Id,
                    Name = t.Name
                });
            foreach (var item in parentModuleData)
            {
                model.ParentModuleItems.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });
            }
        }
    }
}
