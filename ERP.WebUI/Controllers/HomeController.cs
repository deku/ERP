﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ERP.WebUI
{
    public class HomeController : BaseController
    {
        //
        // GET: /Home/
        [AdminLayout]
        [AdminPermission(PermissionCustomMode.Ignore)]
        public ActionResult Index()
        {
            UserRepository userRepository = new UserRepository();
            //User user = new User();
            //user.UserName = "Bryan";
            //user.LoginName = "bryan";
            //user.Password = "123";
            //user.Address = "gz";
            //userRepository.CreateUser(user);
            return View();
        }

    }
}
