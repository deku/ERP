﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ERP.WebUI.Controllers
{
    [AdminPermission(PermissionCustomMode.Ignore)]
    public class ErrorController : Controller
    {
        //
        // GET: /Common/Error/

        public ActionResult Page400()
        {
            return View();
        }

        public ActionResult Page404()
        {
            return View();
        }

        public ActionResult Page500()
        {
            return View();
        }

    }
}
