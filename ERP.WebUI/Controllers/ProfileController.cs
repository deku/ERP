﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ERP.WebUI
{
    [AdminPermission(PermissionCustomMode.Ignore)]
    public class ProfileController : BaseController
    {
        [AdminLayout]
        public ActionResult Index()
        {
            var entity = this.GetCurrentUser();
            var model = new ProfileModel
            {
                Id = entity.Id,
                LoginName = entity.LoginName,
                Email = entity.Email,
                FullName = entity.FullName,
                Phone = entity.Phone,
                LoginCount = entity.LoginCount,
                LastLoginTime = entity.LastLoginTime,
                RegisterTime = entity.RegisterTime
            };
            return View(model);
        }

        [AdminLayout]
        public ActionResult ChangePwd()
        {
            var entity = this.GetCurrentUser();
            var model = new ChangePwdModel
            {
                Id = entity.Id,
                LoginName = entity.LoginName,
                Email = entity.Email
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePwd(ChangePwdModel model)
        {
            if (ModelState.IsValid)
            {
                UserRepository userRepository = new UserRepository();
                OperationResult result = userRepository.Update(model);
                if (result.ResultType == OperationResultType.Success)
                {
                    return Json(result);
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult CheckPwd(string oldLoginPwd)
        {
            bool result = true;
            var user = SessionHelper.GetSession("CurrentUser") as User;
            if (DESProvider.DecryptString(user.LoginPwd) != oldLoginPwd)
            {
                result = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
