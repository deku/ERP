﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ERP.WebUI.Controllers
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AppendixController : BaseController
    {
        //
        // GET: /Appendix/
        [AdminPermission(PermissionCustomMode.Ignore)]
        [AdminLayout]
        public ActionResult Icon()
        {
            return View();
        }

    }
}
