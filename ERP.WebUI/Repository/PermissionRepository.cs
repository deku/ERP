﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ERP.WebUI
{
    public class PermissionRepository 
    {
        private EFDbContext db = new EFDbContext();
        public IQueryable<Permission> Permissions
        {
            get { return db.Permissions; }
        }

        /// <summary>
        /// 复选框数据源
        /// </summary>
        /// <returns></returns>
        public List<KeyValueModel> GetKeyValueList()
        {
            var keyValueList = new List<KeyValueModel>();
            var dataList = Permissions.Where(t => t.Enabled == true && t.IsDeleted == false)
                                .Select(t => new PermissionModel
                                {
                                    Id = t.Id,
                                    Name = t.Name,
                                    OrderSort = t.OrderSort
                                }).OrderBy(t => t.OrderSort).ToList();
            foreach (var data in dataList)
            {
                keyValueList.Add(new KeyValueModel { Text = data.Name, Value = data.Id.ToString() });
            }
            return keyValueList;
        }

        public OperationResult Insert(PermissionModel model)
        {
            var entity = new Permission
            {
                Code = model.Code,
                Icon = model.Icon,
                Name = model.Name,
                Description = model.Description,
                OrderSort = model.OrderSort,
                Enabled = model.Enabled,
                CreateId = model.CreateId,
                CreateBy = model.CreateBy,
                CreateTime = DateTime.Now
            };
            db.Entry<Permission>(entity).State = EntityState.Added;
            db.SaveChanges();
            return new OperationResult(OperationResultType.Success, "添加成功");
        }

        public OperationResult Update(PermissionModel model)
        {
            var entity = new Permission
            {
                Id = model.Id,
                Code = model.Code,
                Icon = model.Icon,
                Name = model.Name,
                Description = model.Description,
                OrderSort = model.OrderSort,
                Enabled = model.Enabled,
                ModifyId = model.ModifyId,
                ModifyBy = model.ModifyBy,
                ModifyTime = DateTime.Now
            };
            db.Set<Permission>().Attach(entity);
            db.Entry<Permission>(entity).State = EntityState.Modified;
            db.SaveChanges();
            return new OperationResult(OperationResultType.Success, "更新成功");
        }

        public OperationResult Delete(int Id)
        {
            var model = Permissions.FirstOrDefault(t => t.Id == Id);
            model.IsDeleted = true;

            db.Set<Permission>().Attach(model);
            db.Entry<Permission>(model).State = EntityState.Modified;
            db.SaveChanges();
            return new OperationResult(OperationResultType.Success, "删除成功");
        }
    }
}