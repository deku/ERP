﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ERP.WebUI
{
    public class OperateLogRepository 
    {
        private EFDbContext db = new EFDbContext();
        public IQueryable<OperateLog> OperateLogs
        {
            get { return db.OperateLogs; }
        }

        #region 公共方法

        public OperationResult Insert(OperateLogModel model)
        {
            var entity = new OperateLog
            {
                Area = model.Area,
                Controller = model.Controller,
                Action = model.Action,
                Description = model.Description,
                IPAddress = model.IPAddress,
                LoginName = model.LoginName,
                UserId = model.UserId,
                LogTime = model.LogTime
            };
            db.Entry<OperateLog>(entity).State = EntityState.Added;
            db.SaveChanges();
            return new OperationResult(OperationResultType.Success, "添加成功");
        }

        public OperationResult Delete()
        {
            var entities = OperateLogs.Where(t => t.IsDeleted == false);
            foreach (var entity in entities)
            {
                entity.IsDeleted = true;
                db.Set<OperateLog>().Attach(entity);
                db.Entry<OperateLog>(entity).State = EntityState.Modified;
            }
            db.SaveChanges();
            return new OperationResult(OperationResultType.Success, "删除成功");
        }

        #endregion
    }
}