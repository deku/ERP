﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ERP.WebUI
{
    public class UserRoleRepository 
    {
        private EFDbContext db = new EFDbContext();
        public IQueryable<UserRole> UserRoles
        {
            get { return db.UserRoles; }
        }

        public OperationResult Update(UserRole model)
        {
            db.Set<UserRole>().Attach(model);
            db.Entry<UserRole>(model).State = EntityState.Modified;
            db.SaveChanges();
            return new OperationResult(OperationResultType.Success);
        }
    }
}