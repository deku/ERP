namespace ERP.WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ModulePermissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ModuleId = c.Int(nullable: false),
                        PermissionId = c.Int(nullable: false),
                        CreateId = c.Int(),
                        CreateBy = c.String(),
                        CreateTime = c.DateTime(),
                        ModifyId = c.Int(),
                        ModifyBy = c.String(),
                        ModifyTime = c.DateTime(),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Modules", t => t.ModuleId, cascadeDelete: true)
                .ForeignKey("dbo.Permissions", t => t.PermissionId, cascadeDelete: true)
                .Index(t => t.ModuleId)
                .Index(t => t.PermissionId);
            
            CreateTable(
                "dbo.Modules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(),
                        Name = c.String(),
                        LinkUrl = c.String(),
                        Area = c.String(),
                        Controller = c.String(),
                        Action = c.String(),
                        Icon = c.String(),
                        Code = c.String(),
                        OrderSort = c.Int(nullable: false),
                        Description = c.String(),
                        IsMenu = c.Boolean(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        CreateId = c.Int(),
                        CreateBy = c.String(),
                        CreateTime = c.DateTime(),
                        ModifyId = c.Int(),
                        ModifyBy = c.String(),
                        ModifyTime = c.DateTime(),
                        IsDeleted = c.Boolean(),
                        ParentModule_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Modules", t => t.ParentModule_Id)
                .Index(t => t.ParentModule_Id);
            
            CreateTable(
                "dbo.RoleModulePermissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(nullable: false),
                        ModuleId = c.Int(nullable: false),
                        PermissionId = c.Int(),
                        CreateId = c.Int(),
                        CreateBy = c.String(),
                        CreateTime = c.DateTime(),
                        ModifyId = c.Int(),
                        ModifyBy = c.String(),
                        ModifyTime = c.DateTime(),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Modules", t => t.ModuleId, cascadeDelete: true)
                .ForeignKey("dbo.Permissions", t => t.PermissionId)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.ModuleId)
                .Index(t => t.PermissionId);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        OrderSort = c.Int(nullable: false),
                        Icon = c.String(),
                        Description = c.String(),
                        Enabled = c.Boolean(nullable: false),
                        CreateId = c.Int(),
                        CreateBy = c.String(),
                        CreateTime = c.DateTime(),
                        ModifyId = c.Int(),
                        ModifyBy = c.String(),
                        ModifyTime = c.DateTime(),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        OrderSort = c.Int(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        CreateId = c.Int(),
                        CreateBy = c.String(),
                        CreateTime = c.DateTime(),
                        ModifyId = c.Int(),
                        ModifyBy = c.String(),
                        ModifyTime = c.DateTime(),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        CreateId = c.Int(),
                        CreateBy = c.String(),
                        CreateTime = c.DateTime(),
                        ModifyId = c.Int(),
                        ModifyBy = c.String(),
                        ModifyTime = c.DateTime(),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LoginName = c.String(),
                        LoginPwd = c.String(),
                        FullName = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        Enabled = c.Boolean(nullable: false),
                        PwdErrorCount = c.Int(nullable: false),
                        LoginCount = c.Int(nullable: false),
                        RegisterTime = c.DateTime(),
                        LastLoginTime = c.DateTime(),
                        CreateId = c.Int(),
                        CreateBy = c.String(),
                        CreateTime = c.DateTime(),
                        ModifyId = c.Int(),
                        ModifyBy = c.String(),
                        ModifyTime = c.DateTime(),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OperateLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Area = c.String(),
                        Controller = c.String(),
                        Action = c.String(),
                        IPAddress = c.String(),
                        Description = c.String(),
                        LogTime = c.DateTime(),
                        LoginName = c.String(),
                        UserId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.OperateLogs", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.RoleModulePermissions", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.RoleModulePermissions", "PermissionId", "dbo.Permissions");
            DropForeignKey("dbo.ModulePermissions", "PermissionId", "dbo.Permissions");
            DropForeignKey("dbo.RoleModulePermissions", "ModuleId", "dbo.Modules");
            DropForeignKey("dbo.ModulePermissions", "ModuleId", "dbo.Modules");
            DropForeignKey("dbo.Modules", "ParentModule_Id", "dbo.Modules");
            DropIndex("dbo.OperateLogs", new[] { "UserId" });
            DropIndex("dbo.UserRoles", new[] { "RoleId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.RoleModulePermissions", new[] { "PermissionId" });
            DropIndex("dbo.RoleModulePermissions", new[] { "ModuleId" });
            DropIndex("dbo.RoleModulePermissions", new[] { "RoleId" });
            DropIndex("dbo.Modules", new[] { "ParentModule_Id" });
            DropIndex("dbo.ModulePermissions", new[] { "PermissionId" });
            DropIndex("dbo.ModulePermissions", new[] { "ModuleId" });
            DropTable("dbo.OperateLogs");
            DropTable("dbo.Users");
            DropTable("dbo.UserRoles");
            DropTable("dbo.Roles");
            DropTable("dbo.Permissions");
            DropTable("dbo.RoleModulePermissions");
            DropTable("dbo.Modules");
            DropTable("dbo.ModulePermissions");
        }
    }
}
